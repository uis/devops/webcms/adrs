# Use an Architectural Decision Record to record decisions made when building Drupal platform and sites

## Title 
ADR 1: Using an Architectural Decision Record

## Context 

Architectural Decision Records are being used across the Development community as a way for teams to be able to record decisions that they've made as to why particular problems are being solved in one way. There are plenty of ways to be able to do this. We have considered the following: 
* Using Log4brains - this was something used by Lullabot who we saw the original ADR talk about at DrupalCon Lille 2023. 
* Using Lullabot's Gatsby theme to render ADRs that are generated as xx.mdx files via a CLI.
* Using CLI's to generate and Gitlab pages to render.

## Decision 

We will be using a Gitlab repo at https://gitlab.developers.cam.ac.uk/uis/devops/webcms/adrs to hold a folder called adrs and a template.md file and README.txt file explaining how to use the adrs. This was the approach that was less dependent on other technologies, as Devops will be reviewing this within the Tech Lead Forum for an approach as a whole division. 

ADR's will use the following standard that will be held in the template.md and discussed within the README.txt.

We will keep ADRs in the project repository under https://gitlab.developers.cam.ac.uk/uis/devops/webcms/adrs/adrs/adr-DESC-NNN.md. 
In future we can split these within separate folders if required - drupal, tailwind, devops etc

We should use a lightweight text formatting language like Markdown or Textile.

ADRs will be numbered sequentially and monotonically. Numbers will not be reused. Format will be 001, 002, 003 etc

A full title will be like 'adr-establishing_adr_records-001.md'

If a decision is reversed, we will keep the old one around, but mark it as superseded. (It's still relevant to know that it was the decision, but is no longer the decision.)

We will use a format with just a few parts, so each document is easy to digest. The format has just a few parts:

Title These documents have names that are short noun phrases. For example, "ADR 1: Deployment on Ruby on Rails 3.0.10" or "ADR 9: LDAP for Multitenant Integration"

Context This section describes the forces at play, including technological, political, social, and project local. These forces are probably in tension, and should be called out as such. The language in this section is value-neutral. It is simply describing facts.

Decision This section describes our response to these forces. It is stated in full sentences, with active voice. "We will …"
Status A decision may be "proposed" if the project stakeholders haven't agreed with it yet, or "accepted" once it is agreed. If a later ADR changes or reverses a decision, it may be marked as "deprecated" or "superseded" with a reference to its replacement.

Consequences This section describes the resulting context, after applying the decision. All consequences should be listed here, not just the "positive" ones. A particular decision may have positive, negative, and neutral consequences, but all of them affect the team and project in the future.

The whole document should be one or two pages long. We will write each ADR as if it is a conversation with a future developer. This requires good writing style, with full sentences organised into paragraphs. Bullets are acceptable only for visual style, not as an excuse for writing sentence fragments.


## Status 
Agreed - See ticket https://gitlab.developers.cam.ac.uk/uis/devops/webcms/internal-planning/support-and-research/-/issues/32 that discussed options.

## Consequences 

For each large decision taken when building the Cambridge Web Platform, the decisions will be discussed via a Gitlab issue, and then taken into a formal ADR within the above repo to be a record of the decision made between the team. This will require time to create an ADR to formalise by a member of the team. This approach may also be superseeded by a more global Devops approach in future. 

## Deciders

Jenny Dumitrescu, Brent Steward and Jamie Giberti